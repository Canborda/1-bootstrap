$(function(){

    // Tooltip & Popover initialization
    $('[data-bs-toggle="tooltip"]').tooltip();
    $('[data-bs-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    });

    $('#contact').on('show.bs.modal', function(e){
        console.log('>> Modal "contact" will show.')
        $('#contactBtn').removeClass('btn-outline-success');
        $('#contactBtn').addClass('btn-dark');
        $('#contactBtn').prop('disabled', true)
    });
    $('#contact').on('shown.bs.modal', function(e){
        console.log('>> Modal "contact" has shown.')
    });
    $('#contact').on('hide.bs.modal', function(e){
        console.log('>> Modal "contact" will hide.')
        $('#contactBtn').addClass('btn-outline-success');
        $('#contactBtn').removeClass('btn-dark');
        $('#contactBtn').prop('disabled', false)
    });
    $('#contact').on('hidden.bs.modal', function(e){
        console.log('>> Modal "contact" has hidden.')
    });

});